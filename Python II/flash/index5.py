# coding:utf-8

from flask import Flask

app = Flask(__name__)

@app.route('/')
def index():
    return 'Bienvenido a la mejor página del universo !!!'

# Estilo API <---ruta----><-params->
# http://127.0.0.1:5000/alumno/datos/Alex/18


@app.route('/alumno/datos/')
@app.route('/alumno/datos/<nombre>/')
@app.route('/alumno/datos/<nombre>/<int:edad>/')
@app.route('/alumno/datos/<nombre>/<int:edad>/<sexo>')
def alex(nombre="Nombre por defecto",edad="0",sexo=""):
    if(sexo == "H"):
        resultado='Hola señor' + nombre + ' tiene ' + str(edad) + ' años'
    elif(sexo == "M"):
        resultado='Hola señora ' + nombre + ' tiene ' + str(edad) + ' años'
    else:
        resultado='Hola ' + nombre + ' tiene ' + str(edad) + ' años'

    return resultado 


if __name__ == '__main__':
    app.run(debug=True, port=8080)
