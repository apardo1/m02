# coding:utf-8

from flask import Flask

app = Flask(__name__)

@app.route('/')
def index():
    return 'Bienvenido a la mejor página del universo !!!'

# http://127.0.0.1:5000/alumno/datos?nombre=Alex&edad=2000
# http://127.0.0.1:5000/alumno/datos?nombre=Alex
# http://127.0.0.1:5000/alumno/datos?edad=18
# Los parámetro pueden ser opcionales

@app.route('/alumno/datos')
def alex():
    nombre=request.args.get('nombre','Nombre por defecto')
    edad=request.args.get('edad','Años por defecto')
    return 'Hola ' + nombre + ' tienes ' + edad + ' años'


if __name__ == '__main__':
    app.run(debug=True, port=8080)
