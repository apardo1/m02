# coding:utf-8

from flask import Flask

app = Flask(__name__)

@app.route('/')
def index():
    return 'Hello, World!'

if __name__ == '__main__':
    # El puerto por defecto es el 5000
    # Si activamos el modo debug=True nos coge los cambios al vuelo
    # sin reiniciar el servidor
    # Nos muestra los errores con más detalle
    app.run(debug=True, port=5000)
