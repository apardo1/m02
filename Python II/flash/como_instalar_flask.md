**COMO ROOT:**
`
pip install --upgrade pip
pip install Flask
`

**COMO USUARIO NO ROOT:**

Crear directorio:
`mkdir flask` y `cd flask/`

```
Activar entorno virtual (cada vez que reinicies hay que hacer estos pasos)
python3 -m venv venv
```
`
. venv/bin/activate
`

**EJECUTAR EL SERVIDOR:**
`python3 index.py`

**COMPROBAR EN EL NAVEGADOR:** 
http://localhost:5000
