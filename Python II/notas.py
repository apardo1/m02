"""Su funcion es calcular la nota final de los alumnos al acabar el grado medio
"""

################################################################################
#									IMPORT 								       #
################################################################################

################################################################################
#							  VARIABLES GLOBALES 							   #
################################################################################

horas_M01 = 5
horas_M02 = 20
horas_total = 25

################################################################################
#									NIVEL 4 								   #
################################################################################

# Notas de los usuarios del curso
def menu_notas():
    print("Indique cada asigntura con sus correspondientes notas: ")
    print("")

    M01 = int(input("Indique la nota total de módulo 01: "))
    M02 = int(input("Indique la nota total de módulo 02: "))
    M03 = int(input("Indique la nota total de módulo 03: "))
    M04 = int(input("Indique la nota total de módulo 04: "))
    M05 = int(input("Indique la nota total de módulo 05: "))
    M09 = int(input("Indique la nota total de módulo 09: "))

################################################################################
#									NIVEL 3 								   #
################################################################################

M01 = 2
M01 = 4

# Calculamos la nota del modulo M01
def calcular_nota_M01():
    # Calculamos la nota total del modulo M01
    nota_M01 = horas_M01 / horas_total * M01

    # Si algo esta mal volvemos a repetir la funcion
    return calcular_nota_M01


# Calculamos la nota del modulo M02
def calcular_nota_M02():
    # Calculamos la nota total del modulo M01
    nota_M02 = horas_M02 / horas_total * M02

    # Si algo esta mal volvemos a repetir la funcion
    return calcular_nota_M02


################################################################################
#									NIVEL 2 								   #
################################################################################

# Calculamos la nota de la primera parte del grade medio
def calcular_nota_medio_primero():
    # Calculamos la nota total de la primera parte del modulo
    nota_primero = horas_M01 + horas_M02

    # Si algo esta mal volvemos a repetir la funcion
    return calcular_nota_medio_primero


# Calculamos la nota de la segunda parte del grade medio
def calcular_nota_medio_segundo():
    # Calculamos la nota total de la primera parte del modulo
    nota_segundo = horas_M01 / horas_total * nota_M01

    # Si algo esta mal volvemos a repetir la funcion
    return calcular_nota_medio_segundo


# Calculamos la nota notal, sumanda la nota de la primera parte y de la segunda
def calcular_nota_medio_total():
    # Calculamos la nota total del grado medio
    nota_total = nota_primero * nota_segundo

    # Si algo esta mal volvemos a repetir la funcion
    return calcular_nota_medio_total


################################################################################
#								NIVEL 1 (MAIN)								   #
################################################################################

if __name__ == "__main__":

    # Obtenemos la nota de la primera parte del curso
    nota_medio_primero = calcular_nota_medio_primero()
    # Obtenemos la nota de la segunda parte del curso
    nota_medio_segundo = calcular_nota_medio_segundo()
    # Obtenemos la nota total del curso (suma de las 2 anteriores)
    nota_total = calcular_nota_medio_total()

    # Obtenemos el resultado de la operacion
    resultado(nota_total)