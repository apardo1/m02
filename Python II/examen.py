#coding:utf8

"""Su funcion es calcular el area de 2 circulos y un lateral, de tal forma que
podamos calcular el area total de un cilindro el cual puede tener las dos bases
de diferente tampanyo de radio
"""

################################################################################
#									IMPORT 								       #
################################################################################

from math import pow, pi

################################################################################
#							  VARIABLES GLOBALES 							   #
################################################################################

radio_grande = 2
radio_pequenyo = 4
area_lateral = 6

################################################################################
#									NIVEL 3 								   #
################################################################################

# Calculamos la generatriz, ya que es una parametro que necesitamos para poder
def calcular_generatriz(generatriz):
    # Calculo de la generatriz para el lateral
    generatriz = pow(altura,2) + pow(radio_grande - radio_pequenyo,2)

    # Si algo esta mal volvemos a repetir la funcion
    return calcular_geometriz

################################################################################
#									NIVEL 2 								   #
################################################################################

def calcular_area_circulo(radio):
    # Calculo para el circulo pequeño
    circulo = pi * pow(radio,2)

    # Si algo esta mal volvemos a repetir la funcion
    return area_circulo


# Calculamos el area del circulo pequeño
def calcular_area_circulo_pequenyo(radio_pequenyo):
    # Calculo para el circulo pequeño
    circulo_pequenyo = pi * pow(radio_pequenyo,2)

    # Si algo esta mal volvemos a repetir la funcion
    return area_circulo_pequenyo

# Calculamos el area del circulo grande
def calcular_area_circulo_grande(radio_grande):
    # Calculo para el circulo grande
    circulo_grande = pi * pow(radio_grande,2)

    # Si algo esta mal volvemos a repetir la funcion
    return area_circulo_grande

# Calculamos el area del circulo grande
def calcular_area_lateral(radio_grande, radio_pequenyo, lateral):
    # Llamamos a la funcion donde se calcula la generatriz
    generatriz = calcular_geometriz()
    # Calculo del lateral del cilindro
    area_lateral= pi * (radio_grande + radio_pequenyo) * generatriz

    # Si algo esta mal volvemos a repetir la funcion
    return area_lateral

def calcula_area_total(radio_grande, radio_pequenyo, lateral):
    # Llamamos a la funcion donde se calcula la generatriz
    area_total = area_circulo_grande + area_circulo_pequenyo + area_lateral
    # Calculo del lateral del cilindro

    # Si algo esta mal volvemos a repetir la funcion
    return area_lateral

################################################################################
#								NIVEL 1 (MAIN)								   #
################################################################################

if __name__ == "__main__":

    # Parametros del usuario de las medidas de las diferentes partes del objeto
        # radio_grande = 2
        # radio_pequenyo = 4
        # area_lateral = 6

    # Obtenemos el circulo pequeño
	area_circulo_pequenyo = calcular_area_circulo_pequenyo(radio_pequenyo)
	# Obtenemos el circulo grande
	area_circulo_grande = calcular_area_circulo_grande(radio_grande)
    # Obtenemos el lateral del costado
    area_lateral = calcular_area_lateral(lateral)
    # Obtenemos el area total
    area_total = calcula_area_total (area_circulo_grande, area_circulo_pequenyo, area_lateral)
    # Obtenemos el resultado de la operacion
    resultado(area_total)