# coding:utf8

def leer_numero_entero (texto):
	"""Lee un número entero del usuario
	Tiene que ser correcto
	No salimos del bucle hasta que lo sea
	"""
	
	salir = "N"
	
	while (salir == "N"):
		num_ent = input (texto)
		
		if (num_ent.isnumeric()): # Condición de salida
			salir = "S"
		else:
			print("El número introducido no es correcto")
			
	return int (num_ent)


########################################################################

if __name__ == "__main__":
	num1 = leer_numero_enero ("Número 1: ")
	num2 = leer_numero_enero ("Número 2: ")
	print ("El mejor programa del mundo:", num1, num2)