# coding:utf8

# El siguiente script enseña un menu de operaciones, el cual si te equivocas te lo recalca para que sepas que
# no esta correcto. Después te pide que vuelvas a introducir el número incorrecto.
import funcion_numero_entero
from time import sleep
from os import system

def mostrar_texto_menu():
	print ("MENU DE OPERACIÓNES:")
	print ("1) Suma")
	print ("2) Resta")
	print ("3) División")
	print ("4) Multiplicación")
	print ("5) Potencia")
	print ("0) Salir")

def elegir_opcion_menu ():
	# Lee un número entero del usuario
	# Tiene que ser correcto
	# No salimos del bucle hasta que lo sea

	salir = "N"

	# Al importar funcion_leer_numero tenemos que llamarlo
	while (salir == "N"):

		mostrar_texto_menu ()
		opcion = funcion_numero_entero.leer_numero_entero ("Indique la operación que desea con el número correspondiente: ")

		# Limpiamos la pantalla para que no se llene con los menus. De esta manera,
		# cuando el usurio pase de menu o se equivoque y vuleva a salir el menu,
		# desaparecera el menu anterior.
		system("clear")

		if (opcion >= 0) and (opcion <= 5): # Condición de salida
			salir = "S"
		else:
			print ("La opcion introducida no es correcta, intentelo de nuevo")
			print ("")

	# Cuando el usurio ponga un número que no sea correcto, le dara error mostrando un
	# texto en pantalla, y después le saldra de nuevo el texto de la variable opcion
	return opcion

################################################################################

def suma():
	numero1 = int(input ("Indique el primer número de la operación: "))
	numero2 = int(input ("Indique el segundo número de la operación: "))
	numero3 = int(input ("Indique el tercer número de la operación: "))
	total = numero1 + numero2 + numero3
	print ("La suma de", numero1, "," , numero2, "y", numero3, "es", total)

def resta():
	numero1 = int(input ("Indique el primer número de la operación: "))
	numero2 = int(input ("Indique el segundo número de la operación: "))
	numero3 = int(input ("Indique el tercer número de la operación: "))
	total = numero1 - numero2 - numero3
	print ("La resta de", numero1, "," , numero2, "y", numero3 ,"es", total)

def division():
	numero1 = int(input ("Indique el primer número de la operación: "))
	numero2 = int(input ("Indique el segundo número de la operación: "))
	numero3 = int(input ("Indique el tercer número de la operación: "))
	total = numero1 / numero2 / numero3
	print ("La división de", numero1, "," , numero2, "y", numero3 ,"es", total)

def multiplicacion():
	numero1 = int(input ("Indique el primer número de la operación: "))
	numero2 = int(input ("Indique el segundo número de la operación: "))
	numero3 = int(input ("Indique el tercer número de la operación: "))
	total = numero1 * numero2 * numero3
	print ("La multiplicacón de", numero1, "," , numero2, "y", numero3 ,"es", total)

def potencia():
	numero1 = int(input ("Indique el primer número de la operación: "))
	numero2 = int(input ("Indique el segundo número de la operación: "))
	total = numero1 ** numero2
	print ("La multiplicacón de", numero1, "y" , numero2 ,"es", total)

################################################################################

if __name__ == "__main__":
	opcion_menu = elegir_opcion_menu ()

	# Limpiamos la pantalla para que no se llene con los menus. De esta manera,
	# cuando el usurio pase de menu o se equivoque y vuleva a salir el menu,
	# desaparecera el menu anterior.
	system("clear")

	print ("NÚMEROS DE LA OPERACIÓN:")

	if (opcion_menu == 1):
		suma()
		sleep(2)
		system("clear")

	elif (opcion_menu == 2):
		resta()
		sleep(2)
		system("clear")

	elif (opcion_menu == 3):
		division()
		sleep(2)
		system("clear")

	elif (opcion_menu == 4):
		multiplicacion()
		sleep(2)
		system("clear")

	elif (opcion_menu == 5):
		Potencia()
		sleep(2)
		system("clear")

################################################################################
################################################################################

import funcion_numero_entero

def mostrar_texto_menu2():
	print ("")
	print ("MENU DE SALIR O CONTINUAR:")
	print ("1) Continuar")
	print ("0) Salir")


def elegir_opcion_menu2():
	# Lee un número entero del usuario
	# Tiene que ser correcto
	# No salimos del bucle hasta que lo sea

	salir = "N"

	# Al importar funcion_leer_numero tenemos que llamarlo
	while (salir == "N"):
		mostrar_texto_menu2 ()
		opcion2 = funcion_numero_entero.leer_numero_entero ("Indique sin quiere salir o continuar con el número correspondiente: ")
		if (opcion2 == 0): # Condición de salida
			salir = "S"
		elif (opcion2 == 1):
			system("clear")
			mostrar_texto_menu()
		else:
			print ("La opcion introducida no es correcta, intentelo de nuevo")

	from os import system
	system("clear")

	return opcion2

################################################################################

print("Gracias por utilizar mi calculadora!!")

if __name__ == "__main__":
	opcion_menu2 = elegir_opcion_menu2 ()

