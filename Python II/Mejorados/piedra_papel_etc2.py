#coding:utf8

"""Juego de piedra, papel, tijera, lagarto, spock.
"""

################################################################################
#									IMPORT 								       #
################################################################################


# Número random
from random import randint
# Para poder definir un intervalo de tiempo
from time import sleep
# Para poder limpiar entre menus
from os import system

################################################################################
#							  VARIABLES GLOBALES 							   #
################################################################################


################################################################################
#									NIVEL 5 								   #
################################################################################


################################################################################
#									NIVEL 4 								   #
################################################################################

def leer_opcion_teclado ():
	# Lee un número entero del usuario
	# Tiene que ser correcto
	# No salimos del bucle hasta que lo sea

	salir = "N"

	# Al importar funcion_leer_numero tenemos que llamarlo
	while (salir == "N"):
		opcion = int(input("Indique con el número correspondiente la opcion que quiera seleccionar: "))

		# Limpiamos la pantalla para que no se llene con los menus. De esta manera,
		# cuando el usurio pase de menu o se equivoque y vuleva a salir el menu,
		# desaparecera el menu anterior.
		system("clear")

		if (opcion >= 1) and (opcion <= 5): # Condición de salida
			salir = "S"
		else:
			print ("La opcion introducida no es correcta, intentelo de nuevo")
			print ("")
			sleep(2)
			system("clear")

	# Cuando el usurio ponga un número que no sea correcto, le dara error mostrando un
	# texto en pantalla, y después le saldra de nuevo el texto de la variable opcion
	return opcion

################################################################################
#									NIVEL 3 								   #
################################################################################

# Muestra el menu del usuario donde mostramos las opciones
# que puede elegir
def mostrar_menu():
	print("ELIJA UNA OPCION")
	print("1) Piedra")
	print("2) Papel")
	print("3) tijeras")
	print("4) Lagarto")
	print("5) Spock")

def	leer_opcion():
	opcion_usuario = leer_opcion_teclado ()

def aleatorio():
	return randint(1,5) # Si algo esta incorrecto, volvera a la variable return

################################################################################
#									NIVEL 2 								   #
################################################################################

# La opcion que va a elegir el usuario
def leer_opcion_usuario():
	mostrar_menu() # Eresultado = opcion_valida(opcion)l menu que va a ver el usuario
	opcion_usuario = leer_opcion() # Va a leer la opcion del usuario

# La opcion aleatoria de la maquina
def leer_opcion_maquina(): # Va a leer la opcion que ha elegido aleatoriamente la maquina
	opcion_maquina = aleatorio

def opcion_valida(opcion_usuario,opcion_maquina):
    leer_opcion_usuario(opcion_usuario)
    leer_opcion_maquina(opcion_maquina)

    if    (((opcion_usuario == 1) and (opcion_maquina == 3)) or
		  ((opcion_usuario == 2) and (opcion_maquina == 1)) or
		  ((opcion_usuario == 3) and (opcion_maquina == 2)) ):
        print("Has ganado!!!")
    else:
        print("Eres un looser, fuera de mi vista")

################################################################################
#								NIVEL 1 (MAIN)								   #
################################################################################
if __name__=="__main__":

	# Obtenemo la opcion del usuario
	opcion_usuario=leer_opcion_usuario()

	# Obtenemo la opcion de la maquina
	opcion_maquina=leer_opcion_maquina()

	opcion_valida(opcion_usuario,opcion_maquina)
