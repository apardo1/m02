#coding:utf8

"""Calculadora: Sumar, restar, dividir, multiplicar y potenciar.
"""

################################################################################
#									IMPORT 								       #
################################################################################


# Número random
import funcion_numero_entero
# Para poder definir un intervalo de tiempo
from time import sleep
# Para poder limpiar entre menus
from os import system

################################################################################
#							  VARIABLES GLOBALES 							   #
################################################################################


################################################################################
#									NIVEL 5 								   #
################################################################################

def menu_operaciones():
    def suma():
    	numero1 = int(input ("Indique el primer número de la operación: "))
    	numero2 = int(input ("Indique el segundo número de la operación: "))
    	numero3 = int(input ("Indique el tercer número de la operación: "))
    	total = numero1 + numero2 + numero3
    	print ("La suma de", numero1, "," , numero2, "y", numero3, "es", total)

    def resta():
    	numero1 = int(input ("Indique el primer número de la operación: "))
    	numero2 = int(input ("Indique el segundo número de la operación: "))
    	numero3 = int(input ("Indique el tercer número de la operación: "))
    	total = numero1 - numero2 - numero3
    	print ("La resta de", numero1, "," , numero2, "y", numero3 ,"es", total)

    def division():
    	numero1 = int(input ("Indique el primer número de la operación: "))
    	numero2 = int(input ("Indique el segundo número de la operación: "))
    	numero3 = int(input ("Indique el tercer número de la operación: "))
    	total = numero1 / numero2 / numero3
    	print ("La división de", numero1, "," , numero2, "y", numero3 ,"es", total)

    def multiplicacion():
    	numero1 = int(input ("Indique el primer número de la operación: "))
    	numero2 = int(input ("Indique el segundo número de la operación: "))
    	numero3 = int(input ("Indique el tercer número de la operación: "))
    	total = numero1 * numero2 * numero3
    	print ("La multiplicacón de", numero1, "," , numero2, "y", numero3 ,"es", total)

    def potencia():
    	numero1 = int(input ("Indique el primer número de la operación: "))
    	numero2 = int(input ("Indique el segundo número de la operación: "))
    	total = numero1 ** numero2
    	print ("La multiplicacón de", numero1, "y" , numero2 ,"es", total)


################################################################################
#									NIVEL 4 								   #
################################################################################

def elegir_opcion_menu ():
	# Lee un número entero del usuario
	# Tiene que ser correcto
	# No salimos del bucle hasta que lo sea

	salir = "N"

	# Al importar funcion_leer_numero tenemos que llamarlo
	while (salir == "N"):

		opcion = funcion_numero_entero.leer_numero_entero ("Indique la operación que desea con el número correspondiente: ")

		# Limpiamos la pantalla para que no se llene con los menus. De esta manera,
		# cuando el usurio pase de menu o se equivoque y vuleva a salir el menu,
		# desaparecera el menu anterior.
		system("clear")

		if (opcion >= 0) and (opcion <= 5): # Condición de salida
			salir = "S"
		else:
			print ("La opcion introducida no es correcta, intentelo de nuevo")
			print ("")

	# Cuando el usurio ponga un número que no sea correcto, le dara error mostrando un
	# texto en pantalla, y después le saldra de nuevo el texto de la variable opcion
	return opcion

################################################################################
#									NIVEL 3 								   #
################################################################################

# Muestra el menu del usuario donde mostramos las opciones
# que puede elegir
def mostrar_texto_menu():
	print ("MENU DE OPERACIÓNES:")
	print ("1) Suma")
	print ("2) Resta")
	print ("3) División")
	print ("4) Multiplicación")
	print ("5) Potencia")
	print ("0) Salir")

def	leer_opcion():
    opcion_leer = elegir_opcion_menu ()
eee
################################################################################
#									NIVEL 2 								   #
################################################################################

# La opcion que va a elegir el usuario
def leer_opcion_usuario():
	mostrar_texto_menu() # Eresultado = opcion_valida(opcion)l menu que va a ver el usuario
	opcion = leer_opcion() # Va a leer la opcion del usuario

################################################################################
#								NIVEL 1 (MAIN)								   #
################################################################################
if __name__ == "__main__":

	# Obtenemo la opcion del usuario
	opcion_usuario=leer_opcion_usuario()
    niputaidea=menu_operaciones()
