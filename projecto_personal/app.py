# coding:utf-8
"""
Página web multipestaÃ±a:
    home
    mostrar tabla -> Muestra cualquier tabla de la BD potsgres seleccionada
    about
    contact
"""

'''
CAl fer "sudo dnf -y install python3-psycopg2"
Editem lâarxiu pg_hba.conf (nano /var/lib/pgsql/data/pg_hba.conf)
sys
Comentem la lÃ­nia:
 #local   all             all                                     peer

Afegim la lÃ­nea:
local   all             all                                     trust
'''

########################################################################
#                               IMPORT
########################################################################
from flask import Flask, flash, get_flashed_messages, session, redirect, render_template, request, url_for
# Para mostrar errores con flash y bootstrap tenemos los tipos:
#   success, info, warning, danger 

from datetime import datetime
import re
import psycopg2

# Para poder en python2 mostrar campos de la BD de tipo utf8 (ñ,ç,à,á,...)
# En python3 hay que comentar las 4 líneas
import sys
if sys.version_info.major < 3:
    reload(sys)
sys.setdefaultencoding('utf8')


########################################################################
#                   VARIABLES GLOBALES Y CONSTANTES
########################################################################



########################################################################
#                               NIVEL 4
########################################################################



########################################################################
#                               NIVEL 3
########################################################################



########################################################################
#                               NIVEL 2
########################################################################
def control_errores_es_entero(variable_string):
    """
    Comprueba si el string pasado como parámetro es entero

    Returns:
        Boolean -- Si es entero->True
    """

    try:
        int(variable_string)
        es_entero = True
    except ValueError:
        es_entero = False

    return es_entero

def comprobar_errores_clientes(id,nombre,apellidos,DNI,saldo):
    hay_error = False
    
    if id and not id.isnumeric():
        error = 'El nÃºmero de teléfono debe ser numérico'
        flash(error,'danger')
        hay_error = True


    if not nombre:
        error = 'El nombre es obligatorio ponerlo'
        flash(error,'danger')
        hay_error = True
    else:
        if len(nombre) >15:
            error = 'La longitud máxima del nombre son 15 caracteres'
            flash(error,'danger')
            hay_error = True
    
    if not apellidos:
        error = 'El apellidos es obligatorio ponerlo'
        flash(error,'danger')
        hay_error = True
    else:
        if len(apellidos) >15:
            error = 'La longitud máxima del nombre son 15 caracteres'
            flash(error,'danger')
            hay_error = True
    
    if not DNI:
        error = 'El DNI es obligatorio ponerlo'
        flash(error,'danger')
        hay_error = True
    else:
        if len(DNI) >9:
            error = 'La longitud máxima del nombre son 9 caracteres'
            flash(error,'danger')
            hay_error = True

    if not cuentas:
        error = 'El saldo es obligatorio ponerlo'
        flash(error,'danger')
        hay_error = True
    else:
        if len(saldo ) >5:
            error = 'La longitud máxima del saldo son 5 caracteres'
            flash(error,'danger')
            hay_error = True


    
    return hay_error



########################################################################
#                               NIVEL 1 (MAIN)
########################################################################
app = Flask(__name__)

# "secretkey" es necesario para usar flash() para los errores
# Lo hace mdiante la sesión, que se hace con una cookie
# firmada con esta clave para evitar que manipulen su contenido
app.secret_key = 'many random bytes'

########################################################################
@app.route("/")
def home():
    return render_template("home.html")

########################################################################
@app.route("/about/")
def about():
    return render_template("about.html")

########################################################################
@app.route("/contact/")
def contact():
    return render_template("contact.html")

########################################################################
@app.route("/api/data/")
def get_data():
    return app.send_static_file("json/data.json")

########################################################################
@app.route("/mostrar_tabla_bd/", methods=('GET', 'POST'))
def mostrar_tabla_bd():
    # La primera vez que se carga la página del formulario lo hace con GET
    # También nos pueden llamar con GET y la variable de 
    # sesión 'nombre_tabla' informada después de modificar o borrar un registro
    if request.method == 'GET' and not session.get('nombre_tabla'):
        return render_template("mostrar_tabla_bd.html",tot_rows=0)

    # Cuando el usuario ya ha entrado en el formulario y lo ha enviado con
    # el botón "Submit" se hace con POST
    elif request.method == 'POST' or (request.method == 'GET' and session.get('nombre_tabla')) :
        # Nos llaman con GET y la variable de sesión 'nombre_tabla' informada
        # después de modificar o borrar un registro
        if request.method == 'GET' and session.get('nombre_tabla'):
            # Al usar 'pop' en lugar de 'get' , leemos la variable y luego la borramos
            nombre_tabla = session.pop('nombre_tabla')
        else:
            # Es un POST normal
            # Leemos los valores introducidos en el formulario
            nombre_tabla = request.form['nombre_tabla']

        # Si han dejado en blanco el campo "nombre_tabla"
        if not nombre_tabla:
            error = 'El nombre de la tabla es obligatorio ponerlo'
            flash(error,'danger')
            return render_template("mostrar_tabla_bd.html",tot_rows=0)
        else:
            try:
                conn = psycopg2.connect(database="cajero", user="postgres", password="jupiter")
                print("DATABASE OPENED SUCCESSFULLY \n")

            except:
                print("CONNECTION ERROR")
                exit(2)

            cur = conn.cursor()

            try:
                sql=" SELECT * " + \
                    " FROM " + nombre_tabla + \
                    " ORDER BY id "
                cur.execute(sql)

                rows = cur.fetchall() # Obtenemos todas las filas
                tot_rows = rows.__len__() # Obtenemos el nÃºmero total de filas
                tot_columns = rows[0].__len__() # Obtenemos el nÃºmero total de columnas
                list_columns = [columnames[0] for columnames in cur.description] # Obtenemos las cabecceras

            except psycopg2.Error as er :
                # Error=42P01   ->   No existe la tabla en la BD
                if (er.pgcode=="42P01"):
                    error = 'La tabla no existe en la BD'
                    flash(error,'danger')
                    return render_template("mostrar_tabla_bd.html",tot_rows=0)
                else:
                    print("-------- ERROR:", er.pgcode, " -------- \n")
                    exit(2)
                conn.rollback()

            conn.close()

            return render_template("mostrar_tabla_bd.html",
                                tot_rows=tot_rows,rows=rows,
                                tot_columns=tot_columns,list_columns=list_columns,
                                nombre_tabla=nombre_tabla)

########################################################################
@app.route("/anyadir_clientes/", methods=('GET', 'POST'))
def anyadir_clientes():
    # La primera vez que se carga la página del formulario lo hace con GET
    if request.method == 'GET':
        return render_template("anyadir_clientes.html")

    # Cuando el usuario ya ha entrado en el formulario y lo ha enviado con
    # el botón "Submit" se hace con POST
    elif request.method == 'POST':
        # Leemos los valores introduc id's en el formulario
        id = request.form['id']
        nombre    = request.form['nombre']
        apellidos = request.form['apellidos']
        DNI       = request.form['DNI']
        fecha_nacimiento =request.form['fecha_nacimiento']
        saldo    = request.form ['saldo']

        # Inicializamos el error (por defecto no hay error)
        hay_error = False

        hay_error = comprobar_errores_clientes(nombre,id,apellidos,DNI)

        if hay_error:
            return render_template("anyadir_clientes.html",
                                   id=id,
                                   nombre=nombre,
                                   apellidos=apellidos,
                                   DNI=DNI,
                                   fecha_nacimiento=fecha_nacimiento,
                                   saldo=saldo)
        else:    # No hay errores
            try:
                conn = psycopg2.connect(database="cajero", user="postgres", password="jupiter")
                print("DATABASE OPENED SUCCESSFULLY \n")

            except:
                print("CONNECTION ERROR")
                exit(2)

            cur = conn.cursor()

            try:
                if not id:
                    id = "NULL"
                sql=" INSERT INTO clientes (nombre,id) VALUES " + \
                    "  ('" + str(nombre) + "' , " + \
                    "    " + str(id) + " ) "
                cur.execute(sql)
                conn.commit()

            except psycopg2.Error as er :
                print("DATABASE ERROR")
                exit(2)
                conn.rollback()

            conn.close()

            mensaje = 'La creación se ha realizado con éxito'
            flash(mensaje,'success')
            
            session['nombre_tabla'] = "clientes"
            url = "/mostrar_tabla_bd/"
            return redirect(url)

########################################################################
@app.route("/api/modificar_registro_tabla/<nombre_tabla>/<int:id>/")
def modificar_registro_tabla(nombre_tabla,id):
    # Inicializamos el error (por defecto no hay error)
    error = None

    try:
        conn = psycopg2.connect(database="cajero", user="postgres", password="jupiter")
        print("DATABASE OPENED SUCCESSFULLY \n")

    except:
        print("CONNECTION ERROR")
        exit(2)

    cur = conn.cursor()

    try:
        sql=" SELECT * FROM " + nombre_tabla + \
            " WHERE id>=" + str(id)
        cur.execute(sql)

        rows = cur.fetchall() # Obtenemos todas las filas


        if (nombuentasre_tabla == "clientes"):
            id          = rows[0][1]
            nombre             = rows[0][2]  
            apellidos           = rows[0][3]
            DNI                = rows[0][4]
            fecha_nacimiento   = rows[0][5]
            saldo              = rows[0][5]

            url="/modificar_clientes/" + str(id) + "/" + str(nombre) + "/" + str(apellidos) + "/" + str(DNI) + "/" + str(fecha_nacimiento) + "/" + str(saldo) 

        if (nombre_tabla == "cuentas"):   
            id     = rows[0][1]
            id_cuenta       = rows[0][2]
            contrasenya    = rows[0][3]

            url="/modificar_cuentas/" +  str(id ) + "/" + str(contrasenya ) + "/" + str(saldo) + "/" + str(id_cuenta)

        return redirect(url)

    except psycopg2.Error as er :
        print("DATABASE ERROR")
        exit(2)
        conn.rollback()

    conn.close()

########################################################################
@app.route("/modificar_clientes/<int:id>/<nombre>/<apellidos>/<DNI>/<fecha_nacimiento>/<saldo>", methods=('GET', 'POST'))
def modificar_clientes(id,nombre,apellidos,DNI,fecha_nacimiento,saldo):
    # La primera vez = rows[0][1]que se carga la página del formulario lo hace con GET
    if request.method == 'GET':

        # Si el campo fecha de nacimiento (no obligatorio) en la tabla de la BD es NULL
        # se carga en la tabla html con valor "None"
        # Para el formulario, significa que está en blanco
        if fecha_nacimiento == "None":
           fecha_nacimiento = ""


        return render_template("modificar_clientes.html",
                                id=id,
                                nombre=nombre,
                                apellidos=apellidos,
                                DNI=DNI,
                                fecha_nacimiento=fecha_nacimiento,
                                saldo=saldo)

    # Cuando el usuario ya ha entrado en el formulario y lo ha enviado con
    # el botón "Submit" se hace con POST
    elif request.method == 'POST':
        # Leemos los valores introducidos en el formulario
        nombre = request.form['nombre']
        id = request.form['id']

        # Inicializamos el error (por defecto no hay error)
        hay_error = False

        hay_error = comprobar_errores_clientes(nombre,id)

        if hay_error:
            return render_template("modificar_clientes.html",
                                    id=id,
                                    nombre=nombre,
                                    apellidos=apellidos,
                                    DNI=DNI,
                                    fecha_nacimiento=fecha_nacimiento,
                                    saldo=saldo)
        else:    # No hay errores
            try:
                conn = psycopg2.connect(database="cajero", user="postgres", password="jupiter")
                print("DATABASE OPENED SUCCESSFULLY \n")

            except:
                print("CONNECTION ERROR")
                exit(2)

            cur = conn.cursor()

            try:
                if not fecha_nacimiento:
                    fecha_nacimiento = "NULL"
                sql=" UPDATE clientes " + \
                    " SET id = '" + str(id) + "' , " + \
                    "     nombre = " + str(nombre) + \
                    "     apellidos = " + str(apellidos) + \
                    "     DNI = " + str(DNI) + \
                    "     fecha_nacimiento = " + str(fecha_nacimiento) + \
                    "     saldo = " + str(saldo) + \
                    " WHERE id=" + str(id)
                cur.execute(sql)
                conn.commit()

            except psycopg2.Error as er :
                print("DATABASE ERROR")
                exit(2)
                conn.rollback()

            conn.close()

            mensaje = 'La modificación se ha realizado con éxito'
            flash(mensaje,'success')
            
            session['nombre_tabla'] = "clientes"
            url = "/mostrar_tabla_bd/"
            return redirect(url)

########################################################################
@app.route("/api/borrar_registro_tabla/<nombre_tabla>/<int:id>/")
def borrar_registro_tabla(nombre_tabla,id):
    # Inicializamos el error (por defecto no hay error)
    error = None

    try:
        conn = psycopg2.connect(database="cajero", user="postgres", password="jupiter")
        print("DATABASE OPENED SUCCESSFULLY \n")

    except:
        print("CONNECTION ERROR")
        exit(2)

    cur = conn.cursor()

    try:
        sql=" SELECT * FROM " + nombre_tabla + \
            " WHERE id" + str(id)
        cur.execute(sql)

        rows = cur.fetchall() # Obtenemos todas las filas


        if (nombre_tabla == "clientes"):
            id          = rows[0][1]
            nombre             = rows[0][2]  
            apellidos           = rows[0][3]
            DNI                = rows[0][4]
            fecha_nacimiento   = rows[0][5]
            saldo              = rows[0][5]

            url="/borrar_clientes/" +  str(id) + "/" + str(nombre) + "/" + str(apellidos) + "/" + str(DNI) + "/" + str(fecha_nacimiento) + "/" + str(saldo) 

        if (nombre_tabla == "cuentas"):   
            id     = rows[0][1]
            id_cuenta         = rows[0][2]
            contrasenya     = rows[0][3]
            url="/borrar_cuentas/"+ str(id ) + "/" + str(id_cuenta) + "/" + str(contrasenya )

        return redirect(url)

    except psycopg2.Error as er :
        print("DATABASE ERROR")
        exit(2)
        conn.rollback()

    conn.close()

########################################################################
@app.route("/borrar_clientes/<int:id>/<nombre>/<apellidos>/<DNI>/<fecha_nacimiento>/<saldo>", methods=('GET', 'POST'))
def borrar_clientes(id,nombre,apellidos,DNI,fecha_nacimiento,saldo):
    # La primera vez que se carga la página del formulario lo hace con GET
    if request.method == 'GET':
        return render_template("borrar_clientes.html",
                                id=id,
                                nombre=nombre,
                                apellidos=apellidos,
                                DNI=DNI,
                                fecha_nacimiento=fecha_nacimiento,
                                saldo=saldo)

    # Cuando el usuario ya ha entrado en el formulario y lo ha enviado con
    # el botón "Submit" se hace con POST
    elif request.method == 'POST':
        try:
            conn = psycopg2.connect(database="cajero", user="postgres", password="jupiter")
            print("DATABASE OPENED SUCCESSFULLY \n")

        except:
            print("CONNECTION ERROR")
            exit(2)

        cur = conn.cursor()

        try:
            sql=" DELETE FROM clientes " + \
                " WHERE id=" + str(id)
            cur.execute(sql)
            conn.commit()

        except psycopg2.Error as er :
            # Error=23503   ->   Violación de integridad por clave foránea
            if (er.pgcode=="23503"):
                error = 'No se puede borrar un empleado con cuentas asignados. Primero borre todos los cuentas asignados y luego el empleado.'
                flash(error,'danger')

                session['nombre_tabla'] = "clientes"
                url = "/mostrar_tabla_bd/"
                return redirect(url)
            else:
                print("-------- ERROR:", er.pgcode, " -------- \n")
                exit(2)
            conn.rollback()

        conn.close()

        mensaje = 'El borrado se ha realizado con éxito'
        flash(mensaje,'success')
        
        session['nombre_tabla'] = "clientes"
        url = "/mostrar_tabla_bd/"
        return redirect(url)

########################################################################
if __name__ == "__main__":
    app.run(debug=True, port=5000)