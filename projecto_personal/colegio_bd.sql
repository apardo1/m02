-- Descargar este archivo a /tmp
-- # psql
-- postgres=# \i /tmp/bd.sql

--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;
SET search_path = public, pg_catalog;
SET default_tablespace = '';
SET default_with_oids = false;

DROP DATABASE IF EXISTS colegio;
CREATE DATABASE colegio;

\c colegio

DROP TABLE IF EXISTS alumnos;
DROP TABLE IF EXISTS clases;


CREATE TABLE clases (
    id SERIAL,
    tutor   character varying(25) NOT NULL,
    nom_clase  character varying(25) NOT NULL,
    curso   character varying(25) NOT NULL,
    PRIMARY KEY (id)

);

CREATE TABLE alumnos (
    id SERIAL,
    nombre character varying(15) NOT NULL,
    apellido character varying(15) NOT NULL,
    DNI character varying (9) NOT NULL,
    fecha_nacimiento date  NULL,
    clase smallint NOT NULL ,
    PRIMARY KEY (id),
    FOREIGN KEY (clase) REFERENCES clases(id)
);

ALTER TABLE public.alumnos OWNER TO postgres;





ALTER TABLE public.clases OWNER TO postgres;

insert into clases (id, tutor,nom_clase,curso ) values
       ( 961,   'Kim',     '1jism',   'informatica' ),
       ( 211,   'Eduard',  '1acim',   'fusteria'    ),
       ( 710,   'Alex',    '1hism',   'informatica' ),
       ( 207,   'Sergi',   '1fism',   'deporte'     ),
       ( 101,   'Pilar',   '2wism',   'informatica' ),
       ( 821,   'Gimenez', '1sism',   'fotografia'  )

;


 Insert into alumnos (nombre, apellido, DNI, fecha_nacimiento,clase) values  
        (  'Arbeloa',  'Sanchez',   '28636332F',  '2002-04-07',  821 ),
        (  'Alba',     'Jordan',    '34343454E',  '1999-09-27',  710 ),
        (  'Jordi',    'Martinez',  '43655456K',  '2003-03-16',  211 ),
        (  'Ishamu',   'Joseph',    '65627666Q',  '2001-12-15',  961 ),
        (  'Laia',     'Logan',     '22967456A',  '2001-12-15',  821 ),
        (  'Lara',     'Fernando',  '35677327A',  '2000-03-10',  961 )
;


