Instalar psycopg2
     Fedora: sudo dnf -y install python3-psycopg2
     Debian: sudo apt install python3-psycopg2
Editamos el archivo pg_hba.conf
     Fedora: sudo nano /var/lib/pgsql/data/pg_hba.conf
     Debian: sudo nano /etc/postgresql/9.6/main/pg_hba.conf

          Comentamos la lÃ­nea:
               #local   all             all                                     peer

          AÃ±adimos:
               local   all             all                                     trust
               host    all             all             127.0.0.1/32            trust
     Reiniciamos el servicio:
          sudo systemctl restart postgresql
